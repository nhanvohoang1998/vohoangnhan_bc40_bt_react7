import { arrTheme } from "../../Themes/ThemeManager"
import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme"
import { ToDoListLightTheme } from "../../Themes/ToDoListLightTheme"
import { ToDoListPrimaryTheme } from "../../Themes/ToDoListPrimaryTheme"
import { add_task, change_theme, delete_task, done_task, edit_task } from "../types/ToDoListTypes"

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 'task-1', taskName: 'task 1', done: true },
    { id: 'task-2', taskName: 'task 2', done: false },
    { id: 'task-3', taskName: 'task 3', done: true },
    { id: 'task-4', taskName: 'task 4', done: true },
  ],
  taskEdit: { id: 'task-4', taskName: 'task 4', done: false },
}

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task:{
      // kiểm tra rỗng
      if(action.newTask.taskName.trim() === ''){
        alert('task name is required!')
        return {...state}
      }
      //kiểm tra tồn tại
      let taskListUpdate = [...state.taskList]
      let index = taskListUpdate.findIndex(task=> task.taskName === action.newTask.taskName)
      if(index !== -1){
        alert("task name already exist!")
        return {...state}
      }

      taskListUpdate.push(action.newTask)
      //xử lý xong thì gán taskList mới vào taskList
      state.taskList = taskListUpdate
      return {...state}

    }
    case change_theme:{

      let theme = arrTheme.find(theme=>theme.id == action.themeId)
      if(theme){
        state.themeToDoList = {...theme.theme}
      }
      return {...state}
    }

    case done_task: {
      let taskListUpdate = [...state.taskList]
      let index = taskListUpdate.findIndex(task=> task.id === action.taskId)
      if(index !== -1){
        taskListUpdate[index].done = true;
      }
      return {...state, taskList:taskListUpdate}
    }

    case delete_task: {
      return {...state, taskList: state.taskList.filter(task => task.id !== action.taskId)}
    }

    case edit_task: {
      return {...state, taskEdit:action.task}
    }
    default:
      return state
  }
}
