import React, { Component } from 'react'
import { ThemeProvider } from 'styled-components'
import { Container } from '../Containers/Container'
import { ToDoListDarkTheme } from '../Themes/ToDoListDarkTheme'
import { ToDoListLightTheme } from '../Themes/ToDoListLightTheme'
import { ToDoListPrimaryTheme } from '../Themes/ToDoListPrimaryTheme'
import { Dropdown } from '../ComponentsToDoList/Dropdown'
import { Heading1, Heading2, Heading3, Heading4, Heading5 } from '../ComponentsToDoList/Heading'
import { TextField } from '../ComponentsToDoList/TextField'
import { Button } from '../ComponentsToDoList/Button'
import { Table, Tr, Th, Thead, Tbody } from '../ComponentsToDoList/Table'
import { connect } from 'react-redux'
import { actionName, addTaskAction, changeThemeAction, deleteTaskAction, doneTaskAction, editTaskAction } from '../redux/actions/ToDoListAction'
import { arrTheme } from '../Themes/ThemeManager'
import { change_theme } from '../redux/types/ToDoListTypes'

class ToDoList extends Component {
  state = {
    taskName: ''
  }

  renderTaskToDo = () => {
    return this.props.taskList.filter(task => !task.done).map((task, index) => {
      return (
        <Tr>
          <Th>{task.taskName}</Th>
          <Th className='text-right'>
            <Button onClick={()=>{this.props.dispatch(editTaskAction(task))}}><i className='fa fa-edit'></i></Button>
            <Button onClick={()=>{this.props.dispatch(doneTaskAction(task.id))}}><i className='fa fa-check'></i></Button>
            <Button onClick={()=>{this.props.dispatch(deleteTaskAction(task.id))}}><i className='fa fa-trash'></i></Button>
          </Th>
        </Tr>
      )
    })
  }

  renderTaskCompleted = ()=>{
    return this.props.taskList.filter(task=>task.done).map((task,index)=>{
      return(
        <Tr>
                    <Th>{task.taskName}</Th>
                    <Th className='text-right'>
                      <Button onClick={()=>{this.props.dispatch(deleteTaskAction(task.id))}}><i className='fa fa-trash'></i></Button>
                    </Th>
                  </Tr>
      )
    })
  }

  renderTheme = ()=>{
    return arrTheme.map((theme,index)=>{
      return <option value={theme.id}>{theme.name}</option>
    })
  }

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className='w-50' style={{ backgroundColor: `${this.props.themeToDoList.bgColor}` }}>
          <Dropdown onChange={(e)=>{
            let {value} = e.target;
            //dispatch value lên reducer
            this.props.dispatch(changeThemeAction(value))
          }}>
            {this.renderTheme()}
          </Dropdown>
          <Heading3>To do list</Heading3>
          <TextField value={this.props.taskEdit.taskName} name='taskName' onChange={(e)=>{
            this.setState({
              taskName: e.target.value
            })
          }} label="task name" className="w-50"></TextField>
          <Button onClick={()=>{
            // lấy thông tin người nhập vào input
            let {taskName} = this.state

            // tạo ra 1 task object
            let newtask = {
              id: Date.now(),
              taskName: taskName,
              done: false,
            }

            //đưa task object lên reudx thông qua phương thức dispatch
            this.props.dispatch(addTaskAction(newtask))



          }} className='ml-2'><i className='fa fa-plus'></i>Add task</Button>
          <Button className='ml-2'><i className='fa fa-upload'></i>Update task</Button>

          <hr />
          <Heading3>Task to do</Heading3>
          <Table>
            <Thead>
              {this.renderTaskToDo()}
            </Thead>
          </Table>

          <Heading3>Task completed</Heading3>
          <Table>
            <Thead>
              {this.renderTaskCompleted()}
            </Thead>
          </Table>
        </Container>
      </ThemeProvider>
    )
  }
}

const mapStateToProps = state => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit
  }
}

export default connect(mapStateToProps, null)(ToDoList)
